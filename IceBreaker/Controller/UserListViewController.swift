//
//  UserListViewController.swift
//  IceBreaker
//
//  Created by Rohan Mayya on 11/01/19.
//  Copyright © 2019 Puneeth K. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class UserListViewController: UITableViewController {
    
    
    let fireStoreDB = Firestore.firestore()
    
    let cellId = "cellId"
    var users = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        fetchUser()
        
    }
    
    func fetchUser(){
        users.removeAll()
        fireStoreDB.collection("User").whereField("id", isEqualTo: "IvDEsgWeysc60Pz33pZNuMAnFKB3").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print(document)
                    document.reference.delete()
                }
            }
        }
        fireStoreDB.collection("User").addSnapshotListener { querySnapshot, error in
                guard let documents = querySnapshot?.documents else {
                    print("Error fetching documents: \(error!)")
                    return
                }
            
                for document in documents  {
                    let data = document.data() // dictionary in dictionary of users
                    // as of now, documentID is the same as ID property for each user. to refactor, retrieve documentID from each document and append it to data dictionary and then use setValuesForKeys. this will enable us to remove the ID property from user table.
                    print(data)
                    print(self.users)
                    let user = User()
                    user.setValuesForKeys(data)
                    self.users.append(user)

                    self.tableView.reloadData()
                }


        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        //memory efficiency
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        let user = users[indexPath.row]
        
        cell.textLabel?.text = user.name
        cell.detailTextLabel?.text = user.email

        
        if let profileImageUrl = user.profileImage {
            cell.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatController=ChatViewController(collectionViewLayout:UICollectionViewFlowLayout())
        let user=self.users[indexPath.row]
        chatController.user=user
        navigationController?.pushViewController(chatController, animated: true)
    }
    
}



