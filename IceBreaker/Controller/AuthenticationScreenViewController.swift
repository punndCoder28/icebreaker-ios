//
//  ViewController.swift
//  IceBreaker
//
//  Created by Puneeth K on 27/11/18.
//  Copyright © 2018 Puneeth K. All rights reserved.
//

import UIKit
import Firebase
import SwiftKeychainWrapper
import GoogleSignIn


class AuthenticationScreenViewController: UIViewController, GIDSignInUIDelegate {
    

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
      
            
        signInButton.layer.cornerRadius=7
        signUpButton.layer.borderColor=UIColor(red: 116/255, green: 185/255, blue: 255/255, alpha: 1).cgColor
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //to keep the user signed in in the particular device
        if KeychainWrapper.standard.string(forKey: "KEY_UID") != nil{
                    performSegue(withIdentifier: "toHomePage", sender: nil)
                }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        email.text=""
        password.text=""
    }
    
    
    @IBAction func signIn(_ sender: Any) {
        if let email=email.text,let password=password.text{
            Auth.auth().signIn(withEmail: email, password: password)
            { (user,error) in
                if error != nil{
                    //go to signup with an error
                    self.alert(title: "ERROR", message: "Invalid email/password")
                }else{
                    //checks if the user has a user id and the performs the navigation
                    if let userID=user?.user.uid{
                        print(userID)
                        KeychainWrapper.standard.set((user?.user.uid)!, forKey: "KEY_UID")
                        self.performSegue(withIdentifier: "toHomePage", sender: nil)
                    }
                
                }
            }
        }
    }
    
    func alert(title:String,message:String){
        let alert=UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in
            alert.dismiss(animated: true, completion: nil)
            //completion to go to a different page when ok is pressed
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        present(alert,animated: true,completion: nil)
    }
    
    
    
}

