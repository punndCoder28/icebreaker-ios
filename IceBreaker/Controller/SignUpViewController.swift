//
//  SignUpViewController.swift
//  IceBreaker
//
//  Created by Puneeth K on 27/11/18.
//  Copyright © 2018 Puneeth K. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import SwiftKeychainWrapper

class SignUpViewController: UIViewController {

    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var alreadyUser: UIButton!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    
    
    /*
    Done. Created userID collection instead of user so it is easier to access data of the user
     */
    
    var fireStoreDB = Firestore.firestore()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        signUpButton.layer.cornerRadius=7
        alreadyUser.layer.borderColor=UIColor(red: 116/255, green: 185/255, blue: 255/255, alpha: 1).cgColor
    }
    
    func storeUserData(userID: String){
        // Add a new document with a generated id.
        var ref: DocumentReference? = nil
        let docData: [String: Any]=[
            "email": email.text!,
            "id": userID,
            "profileImage": "some",
            "name": username.text!
        ]
        ref = fireStoreDB.collection("User").document(userID)
        
        ref?.setData(docData){ err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
    }
    
    @IBAction func signUp(_ sender: Any) {
        let email=self.email.text
        let password=self.password.text
        if ((email != nil) && (password != nil) && (username != nil)){
            Auth.auth().createUser(withEmail: email!, password: password!) { (authResult, error) in
                // ...
                if self.password.text==self.confirmPassword.text && error==nil{
                    self.storeUserData(userID: (authResult?.user.uid)!)//function to store user data in the database
                    KeychainWrapper.standard.set((authResult?.user.uid)!, forKey: "KEY_UID")
                    self.performSegue(withIdentifier: "toHomePage1", sender: nil)
                }else{
                    self.alert(title: "ERROR", message: (error?.localizedDescription)!)
                }
                
            }
        }
    }
    
    func alert(title:String,message:String){
        let alert=UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        present(alert,animated: true,completion: nil)
    }
    
}
