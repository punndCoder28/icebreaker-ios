//
//  BioViewController.swift
//  IceBreaker
//
//  Created by Rohan Mayya on 07/01/19.
//  Copyright © 2019 Puneeth K. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import SDWebImage


class BioViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var profileImage: UIImageView!
    
    let fireStoreDB=Firestore.firestore()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        profileImageStyles()
        setProfileImage()
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(tapGestureRecognizer)
        
        
    }
    

    
    func profileImageStyles(){
        self.profileImage.layer.borderWidth = 1.0
        self.profileImage.layer.masksToBounds = false
        self.profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        self.profileImage.clipsToBounds = true
    }
    
    // todo: cache this image
    func setProfileImage(){
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        fireStoreDB.collection("User").whereField("id", isEqualTo: uid)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        self.profileImage.sd_setImage(with: URL(string: document.data()["profileImage"] as! String))
                    }
                }
        }

    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        //        let tappedImage = tapGestureRecognizer.view as! UIImageView
        // Your action
        let picker = UIImagePickerController()
        
        picker.delegate = self
        picker.allowsEditing = true
        
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        //extract the image from the info arg
        //print(info)
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            profileImage.image = selectedImage
            storeImage()
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    func storeImage(){
        let imageName = NSUUID().uuidString
        let storageRef = Storage.storage().reference().child("profile_images").child("\(imageName).png")
        
        if let uploadData = self.profileImage.image!.jpegData(compressionQuality: 0.1) {
//        if let uploadData = self.profileImage.image!.pngData() {
        
            storageRef.putData(uploadData, metadata: nil) { (metadata, err) in
                if err != nil {
                    print(err as Any)
                    return
                }
                storageRef.downloadURL(completion: { (url, error) in
                    if error != nil {
                        print(error!.localizedDescription)
                        return
                    }
                    if let profileImageUrl = url?.absoluteString {
                        //getting the document
                        _=self.fireStoreDB.collection("User").document((Auth.auth().currentUser?.uid)!).updateData([
                            "profileImage": profileImageUrl
                            ])
                    }
                })
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        print("cancelled picker")
        dismiss(animated: true, completion: nil)
    }

}
