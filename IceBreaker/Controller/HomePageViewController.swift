import UIKit
import Firebase
import FirebaseFirestore
import GoogleSignIn
import SwiftKeychainWrapper

class HomePageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    let cellID = "cellID"
    var messagesArray = [Message]()
    var messagesDictionary = [String: Message]()
    
    let fireStoreDB = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        checkIfUserLoggedIn()
        
        tableView.register(UserCell.self, forCellReuseIdentifier: cellID)
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellID)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! UserCell
        
        let message = messagesArray[indexPath.row]

        cell.message = message
        
        
        return cell
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
     }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatController=ChatViewController(collectionViewLayout:UICollectionViewFlowLayout())
        let message = messagesArray[indexPath.row]
        
        guard let chatPartnerId = message.chatPartnerId() else {
            return
        }
        
        let ref = fireStoreDB.collection("User").document(chatPartnerId)
        ref.addSnapshotListener { (querySnapshot, error) in
            if error != nil {
                print(error as Any)
            }
            guard let data = querySnapshot?.data() else {
                return
            }
            
            let user = User()
            user.setValuesForKeys(data)
            
            chatController.user = user
            self.navigationController?.pushViewController(chatController, animated: true)

        }
    }
    
    
    // why convert to @objc type? -> for perform function to use. check later
    @objc public func handleLogout(){
        do{
            try Auth.auth().signOut()
            GIDSignIn.sharedInstance()?.signOut()
            GIDSignIn.sharedInstance()?.disconnect()
            self.dismiss(animated: true, completion: nil)
        } catch let error as NSError{
            print(error)
        }
    }
    
    
    func observeUserMessages(){
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let ref = fireStoreDB.collection("UserMessage").document(uid).collection("messagesForUser")
        
        ref.addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                return
            }
            
            for document in documents {
                let messageIds = document.data().keys
                
                
                for messageId in messageIds {
                    let messagesRef = self.fireStoreDB.collection("Message").document(messageId)
                    
                    messagesRef.addSnapshotListener({ (querySnapshot, error) in
                        if error != nil {
                            print(error as Any)
                        }
                        
                        guard let data = querySnapshot?.data() else {
                            return
                        }
                        
                        let messageToStore = Message(dictionary: data as [String : AnyObject])
                        if let chatPartnerID = messageToStore.chatPartnerId(){
                            self.messagesDictionary[chatPartnerID] = messageToStore
                        }
                        self.attemptReloadOfTable()
                        
                        
                    })
                }
            }
        }
    }
    
    
    private func attemptReloadOfTable(){
        self.timer?.invalidate()
        //to stop the profile image from flickering when the view is bought up
        self.timer=Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.reloadTable), userInfo: nil, repeats: false)
    }
    
    var timer: Timer?
    
    @objc func reloadTable(){
        self.messagesArray = Array(self.messagesDictionary.values)
        self.messagesArray.sort(by: { (message1, message2) -> Bool in
            return message1.timestamp > message2.timestamp
        })
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

//    func fetchMessages(){
//
//
//        fireStoreDB.collection("Message").addSnapshotListener { (querySnapshot, error) in
//            guard let messages = querySnapshot?.documents else{
//                print(error?.localizedDescription as Any)
//                return
//            }
//
//            for message in messages {
//                let msg = message.data()
//                let messageToStore = Message(dictionary: msg as [String : AnyObject])
//                messageToStore.setValuesForKeys(msg)
//
//                if let toMessage = messageToStore.toMessage as? String {
//
//                    self.messagesDictionary[toMessage] = messageToStore
//
//                    self.messagesArray = Array(self.messagesDictionary.values)
//                    self.messagesArray.sort(by: { (message1, message2) -> Bool in
//                        return message1.timestamp > message2.timestamp
//                    })
//                }
//
//                self.tableView.reloadData()
//            }
//        }
//    }

    
    
    @IBAction func logOutPressed(_ sender: Any) {
        
        do{
            
            try Auth.auth().signOut()
            GIDSignIn.sharedInstance()?.signOut()
            GIDSignIn.sharedInstance()?.disconnect()
            KeychainWrapper.standard.removeObject(forKey: "KEY_UID")
            self.dismiss(animated: true, completion: nil)
            
        } catch let error as NSError{
            print(error)
        }
        
    }
    
    @IBAction func viewProfilePressed(_ sender: Any) {
        performSegue(withIdentifier: "homePageToProfileSegue", sender: nil)
    }
    

    @IBAction func chatButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "toChatTable", sender: nil)
    }
    
    func checkIfUserLoggedIn(){
        if Auth.auth().currentUser?.uid==nil{
            print("user not logged in")
        }
        else {
            guard let key = Auth.auth().currentUser?.uid else{
            return
        }
            
            messagesArray.removeAll()
            messagesDictionary.removeAll()
            tableView.reloadData()
            
            
            let docRef = fireStoreDB.collection("User").document(key)
            docRef.getDocument { (document, error) in
                if let document = document, document.exists {
                    let docData = document.data()
                    self.navigationItem.title=docData!["name"] as? String
                    
                } else {
                    let docData = document!.data()
                    print(docData!["id"]!)
                }
            }
            observeUserMessages()

        }
    }

}
