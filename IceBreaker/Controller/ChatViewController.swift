//
//  ChatTableViewController.swift
//  IceBreaker
//
//  Created by Puneeth K on 16/01/19.
//  Copyright © 2019 Puneeth K. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class ChatViewController: UICollectionViewController, UITextFieldDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
   
    
    let fireStoreDB = Firestore.firestore()

    
    var user: User? {
        didSet {
            navigationItem.title=user?.name
            
            observeMessages()
            
        }
    }
    
    var messages = [Message]()
    
    func observeMessages(){
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
//        let userMessagesRef = fireStoreDB.collection("UserMessage").document(uid).collection((user?.id)!).whereField("exists", isEqualTo: true)
        let userMessagesRef = fireStoreDB.collection("UserMessage").document(uid).collection("messagesForUser").document((user?.id)!)
        
        userMessagesRef.addSnapshotListener { (querySnapshot, error) in
            if error != nil {
                print(error)
            }
            guard let messageKeys = querySnapshot?.data()?.keys else {
                return
            }
            
            
            for messageId in messageKeys {
                let messagesRef = self.fireStoreDB.collection("Message").document(messageId)
                
                
                messagesRef.addSnapshotListener({ (querySnapshot, error) in
                    guard let data = querySnapshot?.data() else {
                        return
                    }
                    
                    let message = Message(dictionary: data as [String : AnyObject])
                    if message.chatPartnerId() == self.user?.id {
                        self.messages.append(message)
                        self.messages.sort(by: { (message1, message2) -> Bool in
                            return message1.timestamp < message2.timestamp
                        })
                        DispatchQueue.main.async { self.collectionView.reloadData() }
                    }
                })
            }
        }
    }
           
//            for messageID in messageKeys! {
            
                
                
                

        
    
    
    
    
    //                    messagesRef.addSnapshotListener({ (querySnapshot, error) in
    //                        if error != nil {
    //                            print(error as Any)
    //                        }
    //
    //                        guard let data = querySnapshot?.data() else{
    //                            return
    //                        }
    //
    //                        let message = Message(dictionary: data as [String : AnyObject])
    //
    //                        if message.chatPartnerId() == self.user?.id {
    //                            self.messages.append(message)
    //                            self.messages.sort(by: { (message1, message2) -> Bool in
    //                                return message1.timestamp < message2.timestamp
    //                            })
    //                            print("I got here")
    //                            DispatchQueue.main.async { self.collectionView.reloadData() }
    //                        }
    //                    })

    
    let inputTextView: UITextView={
        let messageField=UITextView()
//        messageField.placeholder="Enter message...."
        messageField.backgroundColor=UIColor(red: 200, green: 200, blue: 200, alpha: 1)
        messageField.layer.cornerRadius=10
        messageField.translatesAutoresizingMaskIntoConstraints=false
//        messageField
        messageField.font=UIFont(name: "Avenir Next", size: 17)
        return messageField
    }()
    
    let cellId = "CellID"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.contentInset=UIEdgeInsets(top: 8, left: 0, bottom: 58, right: 0)
        collectionView.scrollIndicatorInsets=UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        collectionView?.alwaysBounceVertical = true
        collectionView.backgroundColor=UIColor.white
        collectionView.register(ChatMessageCell.self, forCellWithReuseIdentifier: cellId)
        
        collectionView.keyboardDismissMode = .interactive
        
        setupInputs()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChatMessageCell
        
        
        cell.textView.text = nil;
        
        
        let message = messages[indexPath.item]
        
        cell.textView.text = message.text
        
        setUpCellBubble(cell: cell, message: message)
        
        if let text = message.text{
            cell.bubbleWidthAnchor?.constant=getEstimatedHeightForTheText(text: message.text!).width+32
        }else if message.imageURL != nil{
            cell.bubbleWidthAnchor?.constant=200;
        }
        
        return cell
    }
    
    //this function is here to make the code more clean
    private func setUpCellBubble(cell: ChatMessageCell, message: Message){
        if let messageImageURL = message.imageURL as? String{
            cell.messageImageView.loadImageUsingCacheWithUrlString(urlString: messageImageURL)
            cell.messageImageView.isHidden=false
            cell.chatBubble.backgroundColor=UIColor.clear
        }else{
            cell.messageImageView.isHidden=true
        }
        
        if message.fromMessage==Auth.auth().currentUser?.uid{
            //make the bubble blue
            cell.chatBubble.backgroundColor=ChatMessageCell.blueCell
            //the cells the get reused so by doing this we make sure the text from the user is always in while color
            cell.textView.textColor=UIColor.white
            cell.bubbleViewRightAnchor?.isActive=true
            cell.bubbleViewLeftAnchor?.isActive=false
        }else{
            //make the bubble grey
            cell.chatBubble.backgroundColor=UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
            cell.textView.textColor=UIColor.black
            cell.bubbleViewRightAnchor?.isActive=false
            cell.bubbleViewLeftAnchor?.isActive=true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height: CGFloat = 80
        
        let message = messages[indexPath.item]
        
        if let textMessage = message.text{
            height=getEstimatedHeightForTheText(text: textMessage).height+20
        }else if let imageWidth = message.imageWidth?.floatValue, let imageHeight = message.imageHeight?.floatValue{
            height = CGFloat(imageHeight/imageWidth*200)
        }
        
        return CGSize(width: view.frame.width, height: height)
    }
    
    private func getEstimatedHeightForTheText(text: String) -> CGRect{
        let size=CGSize(width: 200, height: 1000)
        let options=NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)], context: nil)
    }
    
    
    func setupInputs() {
        let inputView=UIView()
        inputView.backgroundColor=UIColor(red: 242, green: 242, blue: 242, alpha: 1)
        inputView.translatesAutoresizingMaskIntoConstraints=false
        inputView.layer.cornerRadius=6

        view.addSubview(inputView)

        inputView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive=true
        inputView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive=true
        inputView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 0).isActive=true
        inputView.heightAnchor.constraint(equalToConstant: 40).isActive=true

        let sendBtn=UIButton(type: .system)
        sendBtn.setTitle("Send ", for: .normal)
        sendBtn.translatesAutoresizingMaskIntoConstraints=false
        sendBtn.addTarget(self, action: #selector(messageSend), for: .touchUpInside)
        inputView.addSubview(sendBtn)
        sendBtn.leftAnchor.constraint(equalTo: inputTextView.rightAnchor, constant: 0)
        sendBtn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive=true
        sendBtn.centerYAnchor.constraint(equalTo: inputView.centerYAnchor, constant: 0).isActive=true
        sendBtn.widthAnchor.constraint(equalToConstant: 40)
        sendBtn.heightAnchor.constraint(equalTo: inputView.heightAnchor, constant: 0).isActive=true
        sendBtn.titleLabel?.font=UIFont(name: "Avenir Next", size: 20)

        let uploadImage = UIImageView()
        uploadImage.image=UIImage(named: "image2")
        uploadImage.translatesAutoresizingMaskIntoConstraints=false
        uploadImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(uploadPicTapped)))
        uploadImage.isUserInteractionEnabled=true
        inputView.addSubview(uploadImage)
        uploadImage.leftAnchor.constraint(equalTo: inputView.leftAnchor, constant: 10).isActive=true
        uploadImage.centerYAnchor.constraint(equalTo: inputView.centerYAnchor, constant: 0).isActive=true
        uploadImage.widthAnchor.constraint(equalToConstant: 30).isActive=true
        uploadImage.heightAnchor.constraint(equalToConstant: 30).isActive=true

        inputView.addSubview(inputTextView)
        inputTextView.leftAnchor.constraint(equalTo: uploadImage.rightAnchor, constant: 5).isActive=true
        inputTextView.centerYAnchor.constraint(equalTo: inputView.centerYAnchor, constant: 0).isActive=true
//        inputTextView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -50)
        inputTextView.rightAnchor.constraint(equalTo: sendBtn.leftAnchor, constant: 0).isActive=true
        inputTextView.heightAnchor.constraint(equalTo: inputView.heightAnchor, constant: 0).isActive=true

        let seperatorLine=UIView()
        seperatorLine.backgroundColor=UIColor.gray
        seperatorLine.translatesAutoresizingMaskIntoConstraints=false
        inputView.addSubview(seperatorLine)
        seperatorLine.leftAnchor.constraint(equalTo: inputView.leftAnchor, constant: 0).isActive=true
        seperatorLine.topAnchor.constraint(equalTo: inputView.topAnchor, constant: 0).isActive=true
        seperatorLine.widthAnchor.constraint(equalTo: inputView.widthAnchor, constant: 0).isActive=true
        seperatorLine.heightAnchor.constraint(equalToConstant: 1).isActive=true
        
    }
    
    
    @objc func uploadPicTapped(){
        let imagePicker=UIImagePickerController()
        
        imagePicker.allowsEditing=true
        imagePicker.delegate=self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImageToSend: UIImage?
        
        if let editedImage=info[.editedImage] as? UIImage{
            selectedImageToSend=editedImage
        }else if let originalImage=info[.originalImage] as? UIImage{
            selectedImageToSend=originalImage
        }
        
        if let selectedImage=selectedImageToSend{
            uploadToFirebaseStorage(selectedImage: selectedImage)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func uploadToFirebaseStorage(selectedImage: UIImage){
        let imageName=NSUUID().uuidString
        let storageRef = Storage.storage().reference().child("message_images").child(imageName)
        
        if let uploadData = selectedImage.jpegData(compressionQuality: 0.2){
            storageRef.putData(uploadData, metadata: nil) { (metaData, error) in
                if error != nil{
                    print(error?.localizedDescription as Any)
                }
                
               storageRef.downloadURL(completion: { (url, error) in
                    if error != nil{
                        print(error?.localizedDescription as Any)
                    }
                    if let imageURL=url?.absoluteString{
                        self.sendMessageWithURL(imageURL: imageURL, image: selectedImage)
                    }
                })
            }
        }
    }
    
    private func sendMessageWithURL(imageURL: String, image: UIImage){
        let fireStoreRef=Firestore.firestore()
        
        let toId = user?.id
        let fromId = Auth.auth().currentUser?.uid
        let timestamp = NSDate.timeIntervalSinceReferenceDate
        
        let message = ["imageURL": imageURL, "toMessage": toId!, "fromMessage": fromId!, "timestamp": timestamp, "imageWidth": image.size.width, "imageHeight": image.size.height] as [String : Any]
        
        let docRef = fireStoreRef.collection("Message").document()
        
        docRef.setData(message as [String : Any]) { (error) in
            if error != nil {
                print(error as Any)
                return
            }
            
            self.inputTextView.text=nil
            let userMessagesRef = fireStoreRef.collection("UserMessage").document(fromId!)
            
            let messageId = docRef.documentID
            userMessagesRef.setData([messageId: 1], merge: true)
            // hack. use merge = true for multiple field storage under a document (prevent overwrite)
            
            let recipientMessagesRef = fireStoreRef.collection("UserMessage").document(toId!)
            
            recipientMessagesRef.setData([messageId: 1], merge: true)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func messageSend(){
        messages.removeAll()
        
        
        let fireStoreRef=Firestore.firestore()
        
        let toId = user?.id
        let fromId = Auth.auth().currentUser?.uid
        let timestamp = NSDate.timeIntervalSinceReferenceDate
        
        let message = ["text": inputTextView.text!, "toMessage": toId!, "fromMessage": fromId!, "timestamp": timestamp] as [String : Any]
        
        let docRef = fireStoreRef.collection("Message").document()
        
        docRef.setData(message as [String : Any]) { (error) in
            if error != nil {
                print(error as Any)
                return
            }
            
            self.inputTextView.text=nil
            
            
//            let userMessagesRef = fireStoreRef.collection("UserMessage").document(fromId!)
            
            let messageId = docRef.documentID

//            let userMessagesRef = fireStoreRef.collection("UserMessage").document(fromId!).collection(toId!).document(messageId)
            
            let userMessagesRef = fireStoreRef.collection("UserMessage").document(fromId!).collection("messagesForUser").document(toId!)
            
            let docDataForSender: [String: Any] = [
                messageId: 1
            ]
            
            userMessagesRef.setData(docDataForSender, merge: true)
            // hack. use merge = true for multiple field storage under a document (prevent overwrite)
            
            let recipientMessagesRef =  fireStoreRef.collection("UserMessage").document(toId!).collection("messagesForUser").document(fromId!)
            
            let docDataForReciever: [String: Any] = [
                messageId: 1
            ]
            
            recipientMessagesRef.setData(docDataForReciever, merge: true)
        }
    }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
//        messageSend()
//        textField.resignFirstResponder()
//        return true
//    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
}
