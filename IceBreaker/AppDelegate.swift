//
//  AppDelegate.swift
//  IceBreaker
//
//  Created by Puneeth K on 27/11/18.
//  Copyright © 2018 Puneeth K. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import GoogleSignIn
import SwiftKeychainWrapper
import IQKeyboardManagerSwift



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    
    var window: UIWindow?

    var userArray=[User]()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        userArray.removeAll()
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = false
        
        // Any additional options
        // ...
        
        // Enable offline data persistence
        let db = Firestore.firestore()
        db.settings = settings
        IQKeyboardManager.shared.enable=true
        IQKeyboardManager.shared.enableAutoToolbar=false

        GIDSignIn.sharedInstance()?.clientID=FirebaseApp.app()?.options.clientID //setting the sign-in delegate.
        GIDSignIn.sharedInstance()?.delegate=self //setting the sign-in delegate.
        return true
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error=error{
            print(error.localizedDescription)
            return
        }else{
            let authentication = user.authentication
            let credential = GoogleAuthProvider.credential(withIDToken: (authentication?.idToken)!, accessToken: (authentication?.accessToken)!)
            Auth.auth().signInAndRetrieveData(with: credential) { (result, error) in
                if error == nil{
                    KeychainWrapper.standard.set((result?.user.uid)!, forKey: "KEY_UID")
                    self.window?.rootViewController?.performSegue(withIdentifier: "toHomePage", sender: nil)
                }else{
                    print(error?.localizedDescription as Any)
            }
        }
    }
    
    //method to call the handleURL method of the GIDSignIn instance, which will properly handle the URL that your application receives at the end of the authentication process.
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: [:])
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        GIDSignIn.sharedInstance()?.signOut()
    }


}

}
