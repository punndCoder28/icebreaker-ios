//
//  Extensions.swift
//  IceBreaker
//
//  Created by Rohan Mayya on 15/01/19.
//  Copyright © 2019 Puneeth K. All rights reserved.
//


//Caching for images being loaded in UserListViewController

import UIKit

let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
    
    func loadImageUsingCacheWithUrlString(urlString: String){
        
        
        self.image = nil // prevent flickering effect
        
        // check cache for image first
        if let cachedImage = imageCache.object(forKey: urlString as NSString) {
                self.image = cachedImage
                return
            }
        
        
            // otherwise fire off a new download
        guard let url = URL(string: urlString) else{
            return
        }
        
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print(error as Any)
                    return
                }
            
            DispatchQueue.main.async{
                if let downloadedImage = UIImage(data: data!) {
                    imageCache.setObject(downloadedImage, forKey: urlString as NSString)
                    self.image = downloadedImage
                }
            }
        }.resume()
    }
}
