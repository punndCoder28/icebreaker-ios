//
//  User.swift
//  IceBreaker
//
//  Created by Rohan Mayya on 06/01/19.
//  Copyright © 2019 Puneeth K. All rights reserved.
//

import UIKit
import Foundation


class User: NSObject {
    
    @objc var email: String!
    @objc var id: String?
    @objc var profileImage: String?
    @objc var name: String?

}
