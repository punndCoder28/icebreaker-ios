//
//  User.swift
//  IceBreaker
//
//  Created by Rohan Mayya on 06/01/19.
//  Copyright © 2019 Puneeth K. All rights reserved.
//

import UIKit
import Firebase

class Message: NSObject {
    
    @objc var toMessage: String? = ""
    @objc var fromMessage: String? = ""
    @objc var text: String? = ""
    @objc var timestamp: TimeInterval = 0.0
    @objc var imageURL: String? = ""
    @objc var imageWidth: NSNumber? = 0.0
    @objc var imageHeight: NSNumber? = 0.0
    
    
    func chatPartnerId() -> String? {
        
        if fromMessage == Auth.auth().currentUser?.uid {
            return toMessage
        }
        else{
            return fromMessage
        }
    }
    
    init(dictionary: [String: AnyObject]){
        super.init()
        
        fromMessage = dictionary["fromMessage"] as? String
        text = dictionary["text"] as? String
        toMessage = dictionary["toMessage"] as? String
        timestamp = (dictionary["timestamp"] as? TimeInterval)!
        imageURL = dictionary["imageURL"] as? String
        imageWidth = dictionary["imageWidth"] as? NSNumber
        imageHeight = dictionary["imageHeight"] as? NSNumber
    }
}
