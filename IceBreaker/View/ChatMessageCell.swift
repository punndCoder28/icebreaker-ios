//
//  ChatMessageCell.swift
//  IceBreaker
//
//  Created by Rohan Mayya on 20/01/19.
//  Copyright © 2019 Puneeth K. All rights reserved.
//

import UIKit

class ChatMessageCell: UICollectionViewCell {
    
    let textView: UITextView = {
        let tv = UITextView()
        tv.text = "sample"
        tv.font = UIFont.systemFont(ofSize: 16)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor=UIColor.clear
        tv.textColor=UIColor.white
        return tv
    }()
    
    static let blueCell=UIColor(red: 0/255, green: 137/255, blue: 249/255, alpha: 1)
    
    let chatBubble: UIView = {
      let bubble = UIView()
        bubble.backgroundColor=blueCell
        bubble.translatesAutoresizingMaskIntoConstraints=false
        bubble.layer.cornerRadius=15
        bubble.layer.masksToBounds=true
        return bubble
    }()
    
    let messageImageView: UIImageView = {
        let imageView=UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints=false
        imageView.layer.cornerRadius=15
        imageView.layer.masksToBounds=true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    var bubbleWidthAnchor: NSLayoutConstraint?
    var bubbleViewRightAnchor: NSLayoutConstraint?
    var bubbleViewLeftAnchor: NSLayoutConstraint?
    
    override init(frame: CGRect){
        super.init(frame: frame)
        
        addSubview(chatBubble)
        addSubview(textView)
        chatBubble.addSubview(messageImageView)
        
        messageImageView.leftAnchor.constraint(equalTo: chatBubble.leftAnchor, constant: 0).isActive=true
        messageImageView.topAnchor.constraint(equalTo: chatBubble.topAnchor, constant: 0).isActive=true
        messageImageView.widthAnchor.constraint(equalTo: chatBubble.widthAnchor, constant: 0).isActive=true
        messageImageView.heightAnchor.constraint(equalTo: chatBubble.heightAnchor, constant: 0).isActive=true
        
        // ios 9 constraints: x,y,w,h
        bubbleViewRightAnchor=chatBubble.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10)
        bubbleViewRightAnchor?.isActive = true
        bubbleViewLeftAnchor=chatBubble.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10)
        chatBubble.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        bubbleWidthAnchor=chatBubble.widthAnchor.constraint(equalToConstant: 200)
        bubbleWidthAnchor?.isActive = true
        chatBubble.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        // ios 9 constraints: x,y,w,h
        textView.leftAnchor.constraint(equalTo: chatBubble.leftAnchor, constant: 10).isActive=true
        textView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        textView.rightAnchor.constraint(equalTo: chatBubble.rightAnchor, constant: 0).isActive=true
        textView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder){
        fatalError("init(coder:) has not been implemented")
    }
}
